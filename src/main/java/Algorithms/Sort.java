package Algorithms;

import java.util.List;

public class Sort {
    public static int[] bubbleSort(int arr[]){
        // 5 2 1 4 7 3 6
        int j = 0;

        for(int i = 0; j < arr.length; i++){
            if(i == arr.length - 1){
                i = 0;
                j++;
            }
            if(i < arr.length - 1 && arr[i] > arr[i + 1]){
                int temp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = temp;
            }
        }
        return arr;
    }
    public static int[] selectionSort(int arr[]){
        // 5 2 1 4 7 3 6
        int minIndex = 0;
        int j = 0;
        for (int i = 0; j < arr.length; i++){
            if(i < arr.length && arr[i] < arr[minIndex]){
                minIndex = i;
            }
            if(i == arr.length - 1){
                int temp = arr[minIndex];
                arr[minIndex] = arr[j];
                arr[j] = temp;
                i = j;
                j++;
                minIndex = j;
            }
        }
        return arr;
    }
    public static int[] insertionSort(int arr[]){
        // 5 2 1 4 7 3 6
        for (int i = 0; i < arr.length; i++){
           int num = arr[i];
           int j = i - 1;
           while (j >= 0 && arr[j] > num){
                arr[j + 1] = arr[j];
                j--;
           }
           arr[j + 1] = num;
        }
        return arr;
    }

}
