package Algorithms;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
//        System.out.println(Recursion.findFactorial(5));
//        System.out.println(Recursion.findFibonancy(8));
        int[] arr = new int[]{5, 2, 1, 4, 7, 3, 6};
        print(Sort.insertionSort(arr));
    }

    public static void print(int[] arr){
        Arrays.stream(arr).forEach(System.out::print);
    }
}
