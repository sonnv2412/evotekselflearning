package Algorithms;

public class Recursion {
    public static int findFactorial(int num){
        if(num == 2){
            return num;
        }
        return num * findFactorial(num - 1);
    }

    public static int findFibonancy(int num){
        if(num < 2){
            return num;
        }

        return findFibonancy(num - 1) + findFibonancy(num - 2);
    }
}
