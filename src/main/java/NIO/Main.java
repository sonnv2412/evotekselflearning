package NIO;

import java.nio.file.Path;

public class Main {
    public static void main(String[] args) {
        Path file = Path.of("src/main/java/NIO/data.txt");
        char letter = 'e';
        CountLetter count = new CountLetter(file, letter);
        System.out.println(letter  + " occurs " + count.count() + " time(s)");
    }
}
