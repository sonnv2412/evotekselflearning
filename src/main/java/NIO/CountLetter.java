package NIO;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

public class CountLetter {
    private Path file;
    private char letter;

    public CountLetter(Path file, char letter) {
        this.file = file;
        this.letter = letter;
    }

    public int count() {
        int count = 0;
        try (
                InputStream in = Files.newInputStream(file);
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        ) {
            String line = "";
            while ((line = reader.readLine()) != null) {
                for (char item : line.toCharArray()) {
                    if (letter == item) {
                        count++;
                    }
                }
            }
        } catch (IOException i) {
            System.err.println(i.getMessage());
        }
        return count;
    }
}
