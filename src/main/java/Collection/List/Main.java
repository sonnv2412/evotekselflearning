package Collection.List;
import java.io.File;
import java.io.IOException;
import java.util.*;
public class Main {
    public static void main(String[] args) {
        try {
            List<String> lineList = new ArrayList<>();
            Scanner s = new Scanner(new File("src/main/java/Collection/Map/dictionary.txt"));
            while (s.hasNext()) {
                String word = s.next();
                lineList.add(word);
            }
            Random randomGenerator = new Random();

            // Random the number of line to be printed
            int randomNumberLines = randomGenerator.nextInt(10);

            for(int i = 0; i < randomNumberLines; i++){
                // Random line to be printed
                System.out.println(lineList.get(randomGenerator.nextInt(lineList.size() - 1)));
            }
        } catch (IOException e) {
            System.err.println(e);
            System.exit(1);
        }
    }
}
