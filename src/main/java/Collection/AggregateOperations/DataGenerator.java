package Collection.AggregateOperations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class DataGenerator {
    public static List<Person> generatePeopleData(){
        List<Person> people = Arrays.asList(
                new Person("John", Sex.Male, 30, 50000.0),
                new Person("Alice", Sex.Female, 25, 60000.0),
                new Person("Bob", Sex.Male, 35, 70000.0),
                new Person("Emma", Sex.Female, 28, 55000.0),
                new Person("Michael", Sex.Male, 40, 80000.0),
                new Person("Olivia", Sex.Female, 27, 58000.0),
                new Person("David", Sex.Male, 45, 90000.0),
                new Person("Sophia", Sex.Female, 29, 62000.0),
                new Person("James", Sex.Male, 32, 55000.0),
                new Person("Mia", Sex.Female, 26, 65000.0),
                new Person("William", Sex.Male, 38, 75000.0),
                new Person("Charlotte", Sex.Female, 31, 59000.0),
                new Person("Daniel", Sex.Male, 33, 60000.0),
                new Person("Ava", Sex.Female, 24, 63000.0),
                new Person("Matthew", Sex.Male, 37, 72000.0),
                new Person("Emily", Sex.Female, 23, 58000.0),
                new Person("Joseph", Sex.Male, 36, 68000.0),
                new Person("Ella", Sex.Female, 28, 60000.0),
                new Person("Alexander", Sex.Male, 34, 65000.0),
                new Person("Grace", Sex.Female, 30, 62000.0)
        );
        return people;
    }

    public static List<Hobby> generateHobbyData(){
        List<Hobby> hobbies = new ArrayList<>();
        hobbies.add(new Hobby("Reading", "Literature"));
        hobbies.add(new Hobby("Cooking", "Culinary"));
        hobbies.add(new Hobby("Gardening", "Outdoor"));
        hobbies.add(new Hobby("Painting", "Art"));
        hobbies.add(new Hobby("Photography", "Art"));
        hobbies.add(new Hobby("Running", "Fitness"));
        hobbies.add(new Hobby("Swimming", "Fitness"));
        hobbies.add(new Hobby("Cycling", "Fitness"));
        hobbies.add(new Hobby("Yoga", "Fitness"));
        hobbies.add(new Hobby("Hiking", "Outdoor"));
        hobbies.add(new Hobby("Traveling", "Adventure"));
        hobbies.add(new Hobby("Dancing", "Art"));
        hobbies.add(new Hobby("Singing", "Music"));
        hobbies.add(new Hobby("Playing Guitar", "Music"));
        hobbies.add(new Hobby("Playing Piano", "Music"));
        hobbies.add(new Hobby("Drawing", "Art"));
        hobbies.add(new Hobby("Writing", "Literature"));
        hobbies.add(new Hobby("Fishing", "Outdoor"));
        hobbies.add(new Hobby("Birdwatching", "Outdoor"));
        hobbies.add(new Hobby("Scuba Diving", "Adventure"));
        hobbies.add(new Hobby("Skydiving", "Adventure"));
        hobbies.add(new Hobby("Skiing", "Adventure"));
        hobbies.add(new Hobby("Snowboarding", "Adventure"));
        hobbies.add(new Hobby("Surfing", "Adventure"));
        hobbies.add(new Hobby("Meditation", "Wellness"));
        hobbies.add(new Hobby("Gaming", "Entertainment"));
        hobbies.add(new Hobby("Watching Movies", "Entertainment"));
        hobbies.add(new Hobby("Playing Chess", "Games"));
        hobbies.add(new Hobby("Solving Puzzles", "Games"));
        return hobbies;
    }

    public static List<Person> generatePeopleWithHobbyData(){
        List<Hobby> hobbies = generateHobbyData();
        List<Person> people = generatePeopleData();
        int upperBoundOfHobby = hobbies.size() - 1;
        Random randomGenerator = new Random();

        for (var item: people){
            int numberOfHobby = randomGenerator.nextInt(upperBoundOfHobby);
            int countHobby = 0;
            while (countHobby <= numberOfHobby){
                int randomHobbyIndex = randomGenerator.nextInt(upperBoundOfHobby);
                item.getHobbies().add(hobbies.get(randomHobbyIndex));
                countHobby++;
            }
        }
        return people;
    }
}
