package Collection.AggregateOperations;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Person {
    private String name;
    private Sex gender;
    private int age;
    private double salary;
    private Set<Hobby> hobbies;

    public Person(String name, Sex gender, int age, double salary){
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.salary = salary;
        this.hobbies = new HashSet<>();
    }

    public String getName() {
        return name;
    }

    public Set<Hobby> getHobbies() {
        return hobbies;
    }

    public void setHobbies(Set<Hobby> hobbies) {
        this.hobbies = hobbies;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sex getGender() {
        return gender;
    }

    public void setGender(Sex gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}

enum Sex{
    Male, Female, Other
}
