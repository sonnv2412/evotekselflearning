package Collection.AggregateOperations;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchService;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws IOException {
        List<Person> people = DataGenerator.generatePeopleWithHobbyData();
        // Retrieve all Male person
        System.out.println("Male people:");
        people.stream()
                .filter(p -> p.getGender() == Sex.Male)
                .forEach(p -> System.out.print(p.getName() + ", "));
        System.out.println();

        // Retrieve average salary of Male person
        System.out.println("\nAverage salary of male people: " +
            people.stream()
                    .filter(p -> p.getGender() == Sex.Male)
                    .mapToDouble(Person::getSalary)
                    .average()
                    .getAsDouble()
        );

        // Retrieve a map of gender - list of person
        Map<Sex, List<String>> personNameByGender = people.stream()
                .collect(
                        Collectors.groupingBy(
                                Person::getGender, Collectors.mapping(Person::getName, Collectors.toList())
                        )
                );
        System.out.println("\nMap of person by gender");
        personNameByGender.entrySet().stream()
                .forEach(item -> System.out.println(item.getKey() + ": " + item.getValue()));

        // Get person who has salary greater than 50000
        System.out.println("\nPeople who have salary greater than 50000");
        people.stream()
                .filter(p -> p.getSalary() > 50000)
                .forEach(p -> System.out.print(p.getName() + ", "));

        //Get map of person name - hobby list
        System.out.println("\n\nMap of person name - hobby list");
        Map<String, Set<Hobby>> hobbiesByPerson = people.stream()
                .collect(Collectors.groupingBy(
                        Person::getName,
                        Collectors.flatMapping(person -> person.getHobbies().stream(), Collectors.toSet())
                ));

        hobbiesByPerson.forEach((personName, hobbies) -> {
            System.out.print("Person name: " + personName + ", Hobbies: ");
            hobbies.forEach(hobby -> System.out.print("[" + hobby.getName() + "]"));
            System.out.println();
        });
    }
}
