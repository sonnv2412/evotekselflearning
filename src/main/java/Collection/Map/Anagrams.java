package Collection.Map;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Anagrams {
    public static void main(String[] args) {
        int minGroupSize = Integer.parseInt("9");
        // Read words from file and put into a simulated multimap
        Map<String, List<String>> m = new HashMap<String, List<String>>();
        try {
            Scanner s = new Scanner(new File("src/main/java/Collection/Map/dictionary.txt"));
            while (s.hasNext()) {
                String word = s.next();
                String alpha = alphabetize(word);
                List<String> l = m.get(alpha);
                if (l == null)
                    m.put(alpha, l = new ArrayList<String>());
                l.add(word);
            }
        } catch (IOException e) {
            System.err.println(e);
            System.exit(1);
        }

        // Print all permutation groups above size threshold
        for (Map.Entry<String, List<String>> item : m.entrySet()) {
            if (item.getValue().size() >= minGroupSize) {
                System.out.println(item.getKey() + " : " + item.getValue());
            }
        }
    }

    private static String alphabetize(String s) {
        char[] a = s.toCharArray();
        Arrays.sort(a);
        return new String(a);
    }
}
