package Collection.Set;

import java.util.*;

public class FindDups {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("a", "b", "A", "BB", "B");
         final Comparator<String> IGNORE_CASE = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareToIgnoreCase(o2);
            }
        };
        SortedSet<String> s = new TreeSet<>(IGNORE_CASE);
        for (String a : words)
            s.add(a);
        System.out.println(s.size() + " distinct words: " + s);
    }
}
