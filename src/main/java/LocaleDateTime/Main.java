package LocaleDateTime;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class Main {
    public static void main(String[] args) {
        ZonedDateTime zd = ZonedDateTime.parse("2020-05-04T08:05:00Z");
        LocalDateTime ld = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL, FormatStyle.SHORT);
        System.out.println(zd.getMonth() + " " + zd.getDayOfMonth());
        System.out.println("Now: " + ld);
        System.out.println("Formatted Now: " + ld.format(formatter));
    }
}
