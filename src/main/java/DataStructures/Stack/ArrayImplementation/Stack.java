package DataStructures.Stack.ArrayImplementation;

import java.util.Arrays;

public class Stack {
    String[] array;
    private int counter;
    public Stack(){
        array = new String[1];
        counter = 0;
    }

    public String peek(){
        return this.array[this.array.length - 1];
    }

    public void push(String value){
        if(counter == array.length){
            array = Arrays.copyOf(array, array.length + 1);
        }
        array[counter] = value;
        counter++;
    }

    public String pop(){
        String result = peek();
        array[array.length - 1] = null;
        array = Arrays.copyOf(array, array.length - 1);
        return result;
    }

    public void traverse(){
        Arrays.stream(array).forEach(System.out::println);
    }
}
