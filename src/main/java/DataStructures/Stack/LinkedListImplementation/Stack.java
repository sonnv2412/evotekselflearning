package DataStructures.Stack.LinkedListImplementation;

public class Stack {
    Node top;
    Node bottom;
    int length;

    public Stack(){
       top = null;
       bottom = null;
       length = 0;
    }

    public String peek(){
        Node node = this.top;
        top = top.next;
        return  node.value;
    }

    public void push(String value){
        Node newNode = new Node(value);
        if(isEmpty()){
            top = newNode;
            bottom = newNode;
            this.length++;
            return;
        }
        newNode.next = top;
        top = newNode;
        this.length++;
    }

    private boolean isEmpty() {
        return top == null;
    }

    public String pop(){
        String value = top.value;
        top = top.next;
        this.length--;
        return value;
    }

    public void traverse(){
        if(!isEmpty()){
            Node node = this.top;
            while (node != null){
                System.out.println(node.value);
                node = node.next;
            }

            System.out.println("Top: " + top.value);
            System.out.println("Bottom: " + bottom.value);
            System.out.println("Length: " + length);
        }else{
            System.out.println("Empty stack");
        }

    }
}
