package DataStructures.HashTable;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MyHashTable {
    private int size;
    private String[][] data;
    int currentIndex;
    public MyHashTable(int size){
        this.size = size;
        this.data = new String[size][3];
        this.currentIndex = 0;
    }

    public static String hash(String input) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] hashBytes = md.digest(input.getBytes());
        StringBuilder sb = new StringBuilder();

        for (byte b : hashBytes) {
            sb.append(String.format("%02x", b));
        }

        return sb.toString();
    }

    public void set(String key, String value) throws NoSuchAlgorithmException {
        String address = hash(key);
        this.data[currentIndex][0] = address;
        this.data[currentIndex][1] = key;
        this.data[currentIndex][2] = value;
        currentIndex++;
    }

    public String get(String key)  throws NoSuchAlgorithmException {
        String address = hash(key);
        for(int i = 0; i < data.length; i++){
            if(data[i][0].equals(address)){
                return data[i][2];
            }
        }
        return null;
    }

    public void print(){
        for(int i = 0; i < data.length; i++){
            System.out.println(data[i][1] + " : " + data[i][2]);
        }
    }
}
