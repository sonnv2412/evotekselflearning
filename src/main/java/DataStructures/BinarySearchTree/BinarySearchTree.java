package DataStructures.BinarySearchTree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BinarySearchTree {
    Node root;
    public BinarySearchTree(){
        root = null;
    }

    public void insert(int value){
        if(root == null){
            this.root = new Node(value);
            return;
        }
        Node p = root;
        Node parent = root;
        while (p != null){
            parent = p;
            if(p.value == value){
                return;
            }
            if(value < p.value){
                p = p.left;
            }else{
                p = p.right;
            }
        }
        if(parent.value < value){
            parent.right = new Node((value));
        }else{
            parent.left = new Node(value);
        }
    }

    public List<Node> breathFirstSearch(){
        Queue queue = new LinkedList();
        List<Node> result = new ArrayList<Node>();
        Node current;
        queue.add(this.root);
        while (queue.size() > 0){
            current = (Node) queue.remove();
            result.add(current);
            if(current.left != null){
                queue.add(current.left);
            }
            if(current.right != null){
                queue.add(current.right);
            }
        }
        return result;
    }

    public int traverse(Node root){
        int val = root.value;
        System.out.println(val);
        if(root.left != null){
            root = root.left;
            traverse(root.left);
            System.out.println(val);
        }
        if(root.right != null){
            root = root.right;
            traverse(root);
        }

        return val;
    }

   public void preOrder(Node p) {
        if (p == null) {
            return;
        }
         visit(p);
         preOrder(p.left);
         preOrder(p.right);
    }

    public void inOrder(Node p){
        if (p == null) {
            return;
        }
        preOrder(p.left);
        visit(p);
        preOrder(p.right);
    }

    public void postOrder(Node p){
        if (p == null) {
            return;
        }
        preOrder(p.left);
        preOrder(p.right);
        visit(p);
    }

    void visit(Node p){
        System.out.println(p.value);
    }

    //10. Delete
    public void delete(int x) {
        //Tìm node chứa x
        //Phân loại trường hợp xóa node chứa x
        if (root == null) {
            return;
        }
        Node p = root;
        Node parent = null;
        while (p != null) {
            if (p.value == x) {
                break;
            }
            parent = p;
            if (p.value > x) {
                p = p.left;
            } else {
                p = p.right;
            }

        }
        //p == null => không có x trong tree, hoặc p ko chứa x
        if (p == null) {
            return;
        }
        //p chắc chắn khác null
        //---------Trường hợp 1: p không có con------------
        /*
                parent      parent           p
               p                   p
         */
        if (p.left == null && p.right == null) {
            if (parent == null) {
                root = null;
                return;
            }
            if (parent.left == p) {
                parent.left = null;
            } else {
                parent.right = null;
            }
        } //---------Trường hợp 1: p có 1 con------------
        /*
            parent      parent      parent        parent             p        p
           p           p                   p             p          L           R
          L              R               L                  R    */
        if (p.left != null && p.right == null) {
            if (parent == null) {
                root = p.left;
            } else if (parent.left == p) {
                parent.left = p.left;
            } else {
                parent.right = p.left;
            }
        } else if (p.left == null && p.right != null) {
            if (parent == null) {
                root = p.right;
            } else if (parent.left == p) {
                parent.left = p.right;
            } else {
                parent.right = p.right;
            }
        }

        //---------Trường hợp 1: p có 2 con------------
        /*
                p                   p
          p.L                     rm
          ...                   L
             parentRM
                      rm
                   L
         */
        if (p.left != null && p.right != null) {
            //Tìm rightmost của p.L
            //Copy giá trị của rightmost vào p
            //xóa rightmost
            Node rm = p.left;
            Node parentRM = null;
            while (rm.right != null) {
                parentRM = rm;
                rm = rm.right;
            }
            //rm không bao giờ bằng null
            //parentRM có thể là null
            p.value = rm.value;
            if (parentRM == null) {
                p.left = rm.left;
            } else {
                parentRM.right = rm.left;
            }
//            parent.left = p.left;
//            rm.right = p.right;
        }

    }

}
