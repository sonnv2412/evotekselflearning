package DataStructures.BinarySearchTree;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        BinarySearchTree bst = new BinarySearchTree();
//          9
//     4          20
//  1    6     15    170
        bst.insert(9);
        bst.insert(4);
        bst.insert(6);
        bst.insert(20);
        bst.insert(170);
        bst.insert(15);
        bst.insert(1);
//        bst.traverse(bst.root);
//        bst.preOrder(bst.root);
        bst.postOrder(bst.root);
    }
}
