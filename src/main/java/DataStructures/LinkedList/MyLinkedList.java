package DataStructures.LinkedList;

public class MyLinkedList {
    Node head;
    Node tail;
    int length;

    public MyLinkedList(){

    }

    public boolean isEmpty(){
        return head == null;
    }
    public void append(int value){
        Node node = new Node(value);
        if(isEmpty()){
            head = node;
            tail = node;
            tail.next = null;
        }else{
            tail.next = node;
            tail = node;
        }
        length++;
    }
    public void prepend(int value){
        Node node = new Node(value);
        if(isEmpty()){
            head = node;
            tail = node;
            tail.next = null;
        }else{
            node.next = head;
            head = node;
        }
        length++;
    }

    public void remove (int index){
        Node counter = head;
        Node target;
        if(index == 0){
            head = head.next;
            length--;
            return;
        }
        for(int i = 0; i < length; i++){
            if(i == index - 1){
                target = counter.next;
                counter.next = target.next;
                target.next = null;
                target = null;
                length--;
                if(index == length){
                    tail = counter;
                }
                return;
            }
            counter = counter.next;
        }
    }

    public void insert (int index, int value){
        Node node = new Node(value);
        Node start = head;

        if(index == 0){
            prepend(value);
            length++;
            return;
        }
        if(index >= length){
            append(value);
            length++;
            return;
        }
        for(int i = 0; i < length; i++){
            if(i == index - 1){
                node.next = start.next;
                start.next = node;
                length++;
                return;
            }
            start = start.next;
        }
    }

    public void reverse(){
        // 1 2 3 4 5 6

        Node first = this.head;
        this.tail = this.head;
        Node second = this.head.next;
        while (second != null) {
            Node third = second.next;
            second.next = first;
            first = second;
            second = third;
        }
        this.tail.next = null;
        this.head = first;
    }

    public void print(){
        Node node = head;
        System.out.println("Items:");
        while (node != null){
            System.out.println(node.value);
            node = node.next;
        }
        System.out.println("Length: " + this.length );
        System.out.println("Head: " + this.head.value );
        System.out.println("Tail: " + this.tail.value);
    }
}
