package DataStructures.Queue;

import java.util.Stack;

public class MyQueue {
    private Stack stack;
    private Stack reverseStack;

    public MyQueue(){
        stack = new Stack<Integer>();
        reverseStack = new Stack<Integer>();
    }
    public void push(int x) {
        stack = reverseStack(reverseStack);
        stack.push(x);
        reverseStack = reverseStack(stack);

    }

    public int pop() {
        return (int)reverseStack.pop();
    }

    public int peek() {
        return (int)reverseStack.peek();
    }

    public boolean empty() {
        return reverseStack.empty();
    }

    public Stack reverseStack(Stack originalStack){
        Stack outputStack = new Stack<>();
        while (!originalStack.isEmpty()){
            outputStack.push(originalStack.pop());
        }
        return outputStack;
    }
}
