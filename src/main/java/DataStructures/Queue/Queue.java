package DataStructures.Queue;

public class Queue {
    Node first;
    Node last;
    int length;

    public String peek(){
        return this.first.value;
    }

    public void enqueue(String value){
        Node newNode = new Node(value);
        if(isEmpty()){
            first = newNode;
            last = newNode;
            length++;
            return;
        }
        last.next = newNode;
        last = newNode;
        length++;
    }

    public String dequeue(){
        String res = peek();
        first = first.next;
        length--;
        if(length == 0){
            first = null;
            last = null;
        }
        return res;
    }


    private boolean isEmpty() {
        return first == null;
    }
}
