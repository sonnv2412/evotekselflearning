package DataStructures.DoublyLinkedList;

public class MyDoublyLinkedList {
    Node head;
    Node tail;

    int length;

    public MyDoublyLinkedList(){

    }

    public boolean isEmpty(){
        return head == null;
    }
    public void append(int value){
        Node node = new Node(value);
        if(isEmpty()){
            head = node;
            tail = node;
            tail.next = null;
        }else{
            node.prev = tail;
            tail.next = node;
            tail = node;
        }
        length++;
    }
    public void prepend(int value){
        Node node = new Node(value);
        if(isEmpty()){
            head = node;
            tail = node;
            tail.next = null;
        }else{
            head.prev = node;
            node.next = head;
            head = node;
        }
        length++;
    }

    public void remove (int index){
        Node counter = head;
        Node target;
        if(index == 0){
            head = head.next;
            length--;
            return;
        }
        for(int i = 0; i < length; i++){
            if(i == index - 1){
                target = counter.next;
                counter.next = target.next;
                target.next.prev = counter;
                target.prev = null;
                target.next = null;
                target = null;
                length--;
                if(index == length){
                    tail = counter;
                }
                return;
            }
            counter = counter.next;
        }
    }

    public void insert (int index, int value){
        Node node = new Node(value);
        Node start = head;

        if(index == 0){
            prepend(value);
            length++;
            return;
        }
        if(index >= length){
            append(value);
            length++;
            return;
        }
        for(int i = 0; i < length; i++){
            if(i == index - 1){
                start.next.prev = node;
                node.next = start.next;
                node.prev = start;
                start.next = node;
                length++;
                return;
            }
            start = start.next;
        }
    }

    public void print(){
        Node node = head;
        System.out.println("Items:");
        while (node != null){
            System.out.println(node.value);
            node = node.next;
        }
        System.out.println("Length: " + this.length );
        System.out.println("Head: " + this.head.value );
        System.out.println("Tail: " + this.tail.value);
    }
}
