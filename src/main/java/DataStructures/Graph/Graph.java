package DataStructures.Graph;

import java.util.HashMap;
import java.util.HashSet;

public class Graph {
    int numberOfNodes;
    HashMap<Integer, HashSet<Integer>> adjacentList;

    public Graph(){
        numberOfNodes = 0;
        adjacentList = new HashMap<Integer, HashSet<Integer>>();
    }

    public void addVertex(int node){
        adjacentList.put(node, new HashSet<Integer>());
        numberOfNodes++;
    }

    public void addEdge(int node1, int node2){
        adjacentList.get(node1).add(node2);
        adjacentList.get(node2).add(node1);
    }

    public void showConnection(){
        for(Integer key: adjacentList.keySet()){
            System.out.print(key + "-->");
            for (var val:
                 adjacentList.get(key)) {
                System.out.print(val + " ");
            }
            System.out.println();
        }
    }
}
