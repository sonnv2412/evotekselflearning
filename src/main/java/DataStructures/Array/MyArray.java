package DataStructures.Array;

import java.util.Arrays;

public class MyArray {
    private Object[] data;
    private int length;
    private int capacity;
    public MyArray(){
        this.length = 0;
        capacity = 1;
        this.data = new Object[1];
    }

    public Object get(int index){
        return this.data[index];
    }

    public void push(Object item){
        if(capacity == length) {
            this.data = Arrays.copyOf(this.data, this.capacity * 2);
            capacity *= 2;
        }
        this.data[length] = item;
        this.length++;
    }

    public Object pop(){
        Object lastItem = this.data[length - 1];
        this.data[length - 1] = null;
        length--;
        return lastItem;
    }

    public void delete(int index){
        shiftIndex(index);
    }

    public void shiftIndex(int index){
        for (int i = index; i < length - 1; i++){
            data[i] = data[i + 1];
        }
        data[length-1] = null;
        length--;
    }

    public void print(){
        for(int i = 0; i < length; i++){
            System.out.println(i + ": " + data[i]);
        }
    }
}
