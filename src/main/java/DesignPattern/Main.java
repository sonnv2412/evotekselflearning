package DesignPattern;

import DesignPattern.FactoryMethod.Dialog;
import DesignPattern.FactoryMethod.WindowDialog;

public class Main {
    public static void main(String[] args) {
        Dialog dialog = new WindowDialog();
        dialog.renderWindow();
    }
}
