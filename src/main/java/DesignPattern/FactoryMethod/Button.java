package DesignPattern.FactoryMethod;

public interface Button {
    void render();
    void onClick();
}
