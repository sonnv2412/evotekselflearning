package DesignPattern.Builder.Model;

public enum CarType {
    CITY_CAR, SPORT_CAR, SUV
}
