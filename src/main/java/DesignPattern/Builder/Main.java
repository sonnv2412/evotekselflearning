package DesignPattern.Builder;

public class Main {
    public static void main(String[] args) {
        Director director = new Director();
        CarBuilder carBuilder = new CarBuilder();
        director.constructCityCar(carBuilder);
        System.out.println(carBuilder.get());
    }
}
