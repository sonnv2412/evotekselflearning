package NestedClass;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.function.*;
import java.util.stream.Collector;

public class DataStructure {

    // Create an array
    private final static int SIZE = 15;
    private int[] arrayOfInts = new int[SIZE];

    public DataStructure() {
        // fill the array with ascending integer values
        for (int i = 0; i < SIZE; i++) {
            arrayOfInts[i] = i;
        }
    }

    public void printEven() {
        // Print out values of even indices of the array
        DataStructureIterator iterator = this.new EvenIterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();
    }

    // Other implementation of printEven (using Function)
    public void printUsingFunction(Function<Integer, Boolean> iterator) {
        for (int i = 0; i < arrayOfInts.length; i++) {
            if (iterator.apply(i)) {
                System.out.print(arrayOfInts[i] + " ");
            }
        }
    }

    // Other implementation of printEven (using Predicate)
    public void printUsingPredicate(Predicate<Integer> iterator) {
        for (int i = 0; i < arrayOfInts.length; i++) {
            if (iterator.test(i)) {
                System.out.print(arrayOfInts[i] + " ");
            }
        }
    }

    // Other implementation of printEven (using Anonymous class)
    public void printUsingAnonymousClass() {
        // Anonymous class
        DataStructureIterator iterator = new DataStructureIterator() {
            private int nextIndex = 0;

            public boolean hasNext() {
                // Check if the current element is the last in the array
                return (nextIndex <= SIZE - 1);
            }

            public Integer next() {
                // Record a value of an even index of the array
                Integer retValue = Integer.valueOf(arrayOfInts[nextIndex]);

                // Get the next even element
                nextIndex += 2;
                return retValue;
            }
        };
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    interface DataStructureIterator extends java.util.Iterator<Integer> {
    }

    // Inner class implements the DataStructureIterator interface,
    // which extends the Iterator<Integer> interface
    private class EvenIterator implements DataStructureIterator {
        // Start stepping through the array from the beginning
        private int nextIndex = 0;
        public boolean hasNext() {
            // Check if the current element is the last in the array
            return (nextIndex <= SIZE - 1);
        }

        public Integer next() {
            // Record a value of an even index of the array
            Integer retValue = Integer.valueOf(arrayOfInts[nextIndex]);
            // Get the next even element
            nextIndex += 2;
            return retValue;
        }
    }

    public static void main(String s[]) {
        // Fill the array with integer values and print out only
        // values of even indices
        DataStructure ds = new DataStructure();
        // Using lambda expression to define the functional interface
        ds.printUsingPredicate((index) -> index % 2 == 0);
    }
}

