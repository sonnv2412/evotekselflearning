package Generic;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Collection<Integer> ci = Arrays.asList(1, 2, 3, 4);
        int count = MyGeneric.countIf(ci, new OddPredicate());
        System.out.println("Number of odd integers = " + count);
    }
    
    public static void printList(List<?> list){
        for (Object item:
             list) {
            System.out.println(item);
        }
    }

}

class OddPredicate implements UnaryPredicate<Integer> {
    public boolean test(Integer i) { return i % 2 != 0; }
}
